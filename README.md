# Rectangles

### Notes
1. Exercise instructions (with definitions) and visualized scenarios can be found in `/docs`
    * Used `https://www.geogebra.org/m/VWN3g9rE` for plotting rectangles
1. A runnable jar and test report can be found in `/docs`
1. `rectangleB` is being compared against `rectangleA` (`i.e. is rectangleB strictly contained within rectangleA` )
2. `intersects` is `false` if a rectangle is strictly contained within another
3. `strictlyContainedWithin` is `false` for 2 rectangles with same dimensions and if 3 sides are contained within and 4th side is overlapping with the outer line.
4. `containedWithinWithSideOverlap` is `true` for 2 rectangles with same dimensions and if 3 sides are contained within and 4th side is overlapping with the outer line.

### Setup
1. `mvn clean install`
2. `java -jar target/rectangles-0.0.1-SNAPSHOT.jar`

### Technologies
1. Java 8
2. Spring Boot
3. Lombok -- Reduce boilerplate code

### API
`POST http://localhost:8080/api/v1/rectangle/compare`

Sample Request:
```
{
    "rectangleA": {
        "topLeft": {
            "x": 4, "y": 12
        },
        "bottomRight": {
            "x": 11, "y": 5
        }
    },
    "rectangleB": {
        "topLeft": {
            "x": 4, "y": 12
        },
        "bottomRight": {
            "x": 11, "y": 5
        }
    }
}
```

Sample Response:
```
{
    "intersects": true,
    "intersectingPoints": [
        {
            "x": 4, "y": 12
        },
        {
            "x": 11, "y": 5
        },
        {
            "x": 4, "y": 5
        },
        {
            "x": 11, "y": 12
        }
    ],
    "strictlyContainedWithin": false,
    "containedWithinWithSideOverlap": true,
    "adjacency": "NONE"
}
```

cURL Request:
```
curl --location --request POST 'http://localhost:8080/api/v1/rectangle/compare' \
--header 'Content-Type: application/json' \
--data-raw '{
    "rectangleA": {
        "topLeft": {
            "x": 4,
            "y": 12
        },
        "bottomRight": {
            "x": 11,
            "y": 5
        }
    },
    "rectangleB": {
        "topLeft": {
            "x": 4,
            "y": 12
        },
        "bottomRight": {
            "x": 11,
            "y": 5
        }
    }
}'
```

### Future Enhancements
1. Add more documentation/logging
2. Add exception handling (lombok helps avoid null params)
3. Use abstract Shape class to allow extensibility
4. Use Swagger for API docs/client
