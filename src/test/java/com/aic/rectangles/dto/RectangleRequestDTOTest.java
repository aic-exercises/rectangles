package com.aic.rectangles.dto;

import com.aic.rectangles.SetGetTestTemplate;
import com.aic.rectangles.domain.Point;
import com.aic.rectangles.domain.Rectangle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RectangleRequestDTOTest extends SetGetTestTemplate<RectangleRequestDTO> {

    @Test
    @Override
    public void test_setGetVariables() {
        Rectangle rectangle = new Rectangle(new Point(4, 12), new Point(11, 5));
        RectangleRequestDTO requestDTO = new RectangleRequestDTO();

        baseTestSetGetVariables(requestDTO, Rectangle.class, rectangle, "rectangleA");
        baseTestSetGetVariables(requestDTO, Rectangle.class, rectangle, "rectangleB");
    }

    @Test
    public void test_constructor() {
        Rectangle rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB = new Rectangle(new Point(6, 13), new Point(9, 11));

        RectangleRequestDTO requestDTO = new RectangleRequestDTO(rectangleA, rectangleB);

        assertEquals(requestDTO.getRectangleA(), rectangleA);
        assertEquals(requestDTO.getRectangleB(), rectangleB);
    }

}
