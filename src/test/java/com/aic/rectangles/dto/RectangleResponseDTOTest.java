package com.aic.rectangles.dto;

import com.aic.rectangles.SetGetTestTemplate;
import com.aic.rectangles.domain.Adjacency;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RectangleResponseDTOTest extends SetGetTestTemplate<RectangleResponseDTO> {

    @Test
    @Override
    public void test_setGetVariables() {
        RectangleResponseDTO responseDTO = new RectangleResponseDTO();

        baseTestSetGetVariables_boolean(responseDTO, boolean.class, Boolean.FALSE, "isIntersects");
        baseTestSetGetVariables(responseDTO, Set.class, new HashSet<>(), "intersectingPoints");
        baseTestSetGetVariables_boolean(responseDTO, boolean.class, Boolean.FALSE, "isStrictlyContainedWithin");
        baseTestSetGetVariables_boolean(responseDTO, boolean.class, Boolean.FALSE, "isContainedWithinWithSideOverlap");
        baseTestSetGetVariables_boolean(responseDTO, Adjacency.class, Adjacency.NONE, "adjacency");
    }

    @Test
    public void test_allArgsConstructor_and_builder() {
        RectangleResponseDTO responseDTO = new RectangleResponseDTO(
                false, new HashSet<>(), false, true, Adjacency.NONE
        );

        RectangleResponseDTO responseDTO2 = RectangleResponseDTO.builder()
                .intersects(false)
                .intersectingPoints(new HashSet<>())
                .strictlyContainedWithin(false)
                .containedWithinWithSideOverlap(true)
                .adjacency(Adjacency.NONE)
                .build();

        assertEquals(responseDTO, responseDTO2);
    }

}
