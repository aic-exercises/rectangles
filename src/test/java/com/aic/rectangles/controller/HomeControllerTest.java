package com.aic.rectangles.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class HomeControllerTest {

    @Spy
    @InjectMocks
    private HomeController homeController;

    private final String appVersion = "0.0.1-SNAPSHOT";

    @BeforeEach
    public void setup() {
        ReflectionTestUtils.setField(homeController, "appVersion", appVersion);
    }

    @Test
    public void test_home_returnsTextAndVersion() {
        String expected = "Rectangle Service v" + appVersion.split("-")[0];

        assertEquals(expected, homeController.home());
    }
}
