package com.aic.rectangles.controller;

import com.aic.rectangles.domain.Adjacency;
import com.aic.rectangles.domain.Point;
import com.aic.rectangles.domain.Rectangle;
import com.aic.rectangles.dto.RectangleRequestDTO;
import com.aic.rectangles.dto.RectangleResponseDTO;
import com.aic.rectangles.service.RectangleService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

@ExtendWith(MockitoExtension.class)
public class RectangleControllerTest {

    @Spy
    @InjectMocks
    private RectangleController rectangleController;
    @Mock
    private RectangleService mockRectangleService;

    private Rectangle rectangleA, rectangleB;

    @Test
    public void test_compare_returnsResponseDTO_success() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(6, 13), new Point(9, 11));

        RectangleRequestDTO requestDTO = new RectangleRequestDTO(rectangleA, rectangleB);

        RectangleResponseDTO responseDTO = new RectangleResponseDTO(
                true, new HashSet<>(Arrays.asList(new Point(6, 12), new Point(9, 12))),
                false, false, Adjacency.NONE
        );

        doReturn(responseDTO).when(mockRectangleService).compare(rectangleB, rectangleA);

        assertEquals(responseDTO, rectangleController.compare(requestDTO));
    }
}
