package com.aic.rectangles;

import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;

public abstract class SetGetTestTemplate<T> {

    protected void baseTestSetGetVariables(T testObject, Class<?> type, Object testValue, String... fields) {
        for (String field : fields) {
            ReflectionTestUtils.invokeSetterMethod(testObject, field, testValue, type);
            assertEquals(testValue, ReflectionTestUtils.invokeGetterMethod(testObject, field));
        }
    }

    protected void baseTestSetGetVariables_boolean(T testObject, Class<?> type, Object testValue, String... fields) {
        for (String field : fields) {
            String booleanFieldName = field;
            if (field.startsWith("is")) booleanFieldName = field.substring("is".length());

            ReflectionTestUtils.invokeSetterMethod(testObject, booleanFieldName, testValue, type);
            assertEquals(testValue, ReflectionTestUtils.invokeGetterMethod(testObject, field));
        }
    }

    public abstract void test_setGetVariables();
}
