package com.aic.rectangles.domain;

import com.aic.rectangles.SetGetTestTemplate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class PointTest extends SetGetTestTemplate<Point> {

    private Point point;

    @Test
    @Override
    public void test_setGetVariables() {
        point = new Point(1, 2);

        baseTestSetGetVariables(point, int.class, 1, "x");
        baseTestSetGetVariables(point, int.class, 2, "y");
    }

    @Test
    public void test_constructor() {
        point = new Point(3, 6);

        assertEquals(point.getX(), 3);
        assertEquals(point.getY(), 6);
    }
}
