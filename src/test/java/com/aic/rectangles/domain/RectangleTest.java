package com.aic.rectangles.domain;

import com.aic.rectangles.SetGetTestTemplate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class RectangleTest extends SetGetTestTemplate<Rectangle> {

    private Rectangle rectangleA;
    private Point ptTopLeft;
    private Point ptBottomRight;

    @Test
    @Override
    public void test_setGetVariables() {
        ptTopLeft = new Point(2, 5);
        ptBottomRight = new Point(4, 4);
        rectangleA = new Rectangle(ptTopLeft, ptBottomRight);

        baseTestSetGetVariables(rectangleA, Point.class, ptTopLeft, "topLeft");
        baseTestSetGetVariables(rectangleA, Point.class, ptBottomRight, "bottomRight");
    }

    @Test
    public void test_constructor() {
        ptTopLeft = new Point(2, 5);
        ptBottomRight = new Point(4, 4);
        rectangleA = new Rectangle(ptTopLeft, ptBottomRight);

        assertEquals(rectangleA.getTopLeft(), ptTopLeft);
        assertEquals(rectangleA.getBottomRight(), ptBottomRight);
    }

}
