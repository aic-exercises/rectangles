package com.aic.rectangles.service;

import com.aic.rectangles.domain.Adjacency;
import com.aic.rectangles.domain.Point;
import com.aic.rectangles.domain.Rectangle;
import com.aic.rectangles.dto.RectangleResponseDTO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class RectangleServiceImplTest {

    @Spy
    @InjectMocks
    private RectangleServiceImpl rectangleServiceImpl;

    private Rectangle rectangleA, rectangleB;

    @Test
    public void test_compare_returnsProperResponse_whenRectanglesIntersect() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(6, 13), new Point(9, 11));

        RectangleResponseDTO expected = new RectangleResponseDTO(
                true, new HashSet<>(Arrays.asList(new Point(6, 12), new Point(9, 12))),
                false, false, Adjacency.NONE
        );
        RectangleResponseDTO actual = rectangleServiceImpl.compare(rectangleB, rectangleA);

        assertEquals(expected, actual);
    }

    @Test
    public void test_compare_returnsProperResponse_whenRectangleBIsContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(7, 9), new Point(8, 8));

        RectangleResponseDTO expected = new RectangleResponseDTO(
                false, new HashSet<>(), true, true, Adjacency.NONE
        );
        RectangleResponseDTO actual = rectangleServiceImpl.compare(rectangleB, rectangleA);

        assertEquals(expected, actual);
    }

    @Test
    public void test_compare_returnsProperResponse_whenRectangleBIsAdjacentToRectangleA() {
        rectangleA = new Rectangle(new Point(3, 5), new Point(5, 2));
        rectangleB = new Rectangle(new Point(3, 6), new Point(5, 5));

        RectangleResponseDTO expected = new RectangleResponseDTO(
                true, new HashSet<>(Arrays.asList(new Point(5, 5), new Point(3, 5))),
                false, false, Adjacency.PROPER
        );
        RectangleResponseDTO actual = rectangleServiceImpl.compare(rectangleB, rectangleA);

        assertEquals(expected, actual);
    }

    @Test
    public void test_isIntersecting_returnsFalse_whenRectanglesAreAboveEachOther() {
        rectangleA = new Rectangle(new Point(2, 7), new Point(4, 5));
        rectangleB = new Rectangle(new Point(2, 4), new Point(4, 2));

        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB, rectangleA));
        assertFalse(rectangleServiceImpl.isIntersecting(rectangleA, rectangleB));
    }

    @Test
    public void test_isIntersecting_returnsFalse_whenRectanglesAreToTheSidesOfEachOther() {
        rectangleA = new Rectangle(new Point(2, 4), new Point(4, 2));
        rectangleB = new Rectangle(new Point(5, 4), new Point(7, 2));

        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB, rectangleA));
        assertFalse(rectangleServiceImpl.isIntersecting(rectangleA, rectangleB));
    }

    @Test
    public void test_getIntersectingPoints_returnsEmpty_whenRectanglesAreAboveEachOther() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_top = new Rectangle(new Point(6, 16), new Point(9, 15));
        Rectangle rectangleB_bot = new Rectangle(new Point(6, 2), new Point(9, 1));

        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB_top, rectangleA));

        Set<Point> expected = new HashSet<>();
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_top, rectangleA));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_bot, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsEmpty_whenRectanglesAreToTheSidesOfEachOther() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_rgt = new Rectangle(new Point(14, 10), new Point(15, 7));
        Rectangle rectangleB_lft = new Rectangle(new Point(0, 10), new Point(1, 7));

        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB_rgt, rectangleA));
        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB_lft, rectangleA));

        Set<Point> expected = new HashSet<>();
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_rgt, rectangleA));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_lft, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsEmpty_whenRectangleBIsFullyContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB = new Rectangle(new Point(7, 9), new Point(8, 8));

        assertFalse(rectangleServiceImpl.isIntersecting(rectangleB, rectangleA));

        Set<Point> expected = new HashSet<>();
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsSinglePoint_whenRectangleBOverlapsOnACornerOnly() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_topRgt = new Rectangle(new Point(11, 14), new Point(12, 12));
        Rectangle rectangleB_botRgt = new Rectangle(new Point(11, 5), new Point(12, 3));
        Rectangle rectangleB_botLft = new Rectangle(new Point(3, 5), new Point(4, 3));
        Rectangle rectangleB_topLft = new Rectangle(new Point(3, 14), new Point(4, 12));

        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_topRgt, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_botRgt, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_botLft, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_topLft, rectangleA));

        Set<Point> expected;
        expected = new HashSet<>(Collections.singletonList(new Point(11, 12)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_topRgt, rectangleA));

        expected = new HashSet<>(Collections.singletonList(new Point(11, 5)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_botRgt, rectangleA));

        expected = new HashSet<>(Collections.singletonList(new Point(4, 5)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_botLft, rectangleA));

        expected = new HashSet<>(Collections.singletonList(new Point(4, 12)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_topLft, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsTwoPoints_whenRectangleBOverlapsFromOneSideOnly() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_top = new Rectangle(new Point(6, 13), new Point(9, 11));
        Rectangle rectangleB_rgt = new Rectangle(new Point(10, 11), new Point(12, 6));
        Rectangle rectangleB_bot = new Rectangle(new Point(6, 6), new Point(9, 4));
        Rectangle rectangleB_lft = new Rectangle(new Point(3, 11), new Point(5, 6));

        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_top, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_rgt, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_bot, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_lft, rectangleA));

        Set<Point> expected;
        expected = new HashSet<>(Arrays.asList(new Point(6, 12), new Point(9, 12)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_top, rectangleA));

        expected = new HashSet<>(Arrays.asList(new Point(11, 11), new Point(11, 6)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_rgt, rectangleA));

        expected = new HashSet<>(Arrays.asList(new Point(6, 5), new Point(9, 5)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_bot, rectangleA));

        expected = new HashSet<>(Arrays.asList(new Point(4, 11), new Point(4, 6)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_lft, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsFourPoints_whenRectangleBOverlapsOnTwoSides() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_topBot = new Rectangle(new Point(7, 14), new Point(8, 3));
        Rectangle rectangleB_lftRgt = new Rectangle(new Point(2, 9), new Point(13, 8));

        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_topBot, rectangleA));
        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB_lftRgt, rectangleA));

        Set<Point> expected;
        expected = new HashSet<>(Arrays.asList(new Point(7, 12), new Point(8, 12), new Point(7, 5), new Point(8, 5)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_topBot, rectangleA));

        expected = new HashSet<>(Arrays.asList(new Point(4, 9), new Point(11, 9), new Point(4, 8), new Point(11, 8)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB_lftRgt, rectangleA));
    }

    @Test
    public void test_getIntersectingPoints_returnsFourPoints_whenRectanglesAreTheSame() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(4, 12), new Point(11, 5));

        assertTrue(rectangleServiceImpl.isIntersecting(rectangleB, rectangleA));

        Set<Point> expected;
        expected = new HashSet<>(Arrays.asList(new Point(11, 12), new Point(11, 5), new Point(4, 5), new Point(4, 12)));
        assertEquals(expected, rectangleServiceImpl.getIntersectingPoints(rectangleB, rectangleA));
    }

    @Test
    public void test_isFullyContainedWithin_returnsFalse_whenRectangleBIsNotContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(6, 14), new Point(8, 13));
        Rectangle rectangleB_r = new Rectangle(new Point(12, 10), new Point(13, 8));
        Rectangle rectangleB_b = new Rectangle(new Point(7, 4), new Point(9, 3));
        Rectangle rectangleB_l = new Rectangle(new Point(2, 9), new Point(3, 7));

        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_t, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_r, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_b, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isFullyContainedWithin_returnsFalse_whenRectangleBIsPartiallyContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(9, 13), new Point(10, 11));
        Rectangle rectangleB_r = new Rectangle(new Point(10, 7), new Point(12, 6));
        Rectangle rectangleB_b = new Rectangle(new Point(5, 6), new Point(6, 4));
        Rectangle rectangleB_l = new Rectangle(new Point(3, 11), new Point(5, 10));

        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_t, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_r, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_b, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isFullyContainedWithin_returnsFalse_whenRectangleBIsContainedWithinRectangleA_ButOneSideOverlaps() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(6, 12), new Point(8, 11));
        Rectangle rectangleB_r = new Rectangle(new Point(10, 10), new Point(11, 8));
        Rectangle rectangleB_b = new Rectangle(new Point(7, 6), new Point(9, 5));
        Rectangle rectangleB_l = new Rectangle(new Point(4, 9), new Point(5, 7));

        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_t, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_r, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_b, rectangleA));
        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isFullyContainedWithin_returnsFalse_whenRectanglesAreTheSame() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(4, 12), new Point(11, 5));

        assertFalse(rectangleServiceImpl.isFullyContainedWithin(rectangleB, rectangleA));
    }

    @Test
    public void test_isFullyContainedWithin_returnsTrue_whenRectangleBIsFullyContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(6, 9), new Point(9, 8));

        assertTrue(rectangleServiceImpl.isFullyContainedWithin(rectangleB, rectangleA));
    }

    @Test
    public void test_isContainedWithinWithSideOverlap_returnsFalse_whenRectangleBIsNotContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(6, 14), new Point(8, 13));
        Rectangle rectangleB_r = new Rectangle(new Point(12, 10), new Point(13, 8));
        Rectangle rectangleB_b = new Rectangle(new Point(7, 4), new Point(9, 3));
        Rectangle rectangleB_l = new Rectangle(new Point(2, 9), new Point(3, 7));

        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_t, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_r, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_b, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isContainedWithinWithSideOverlap_returnsFalse_whenRectangleBIsPartiallyContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(9, 13), new Point(10, 11));
        Rectangle rectangleB_r = new Rectangle(new Point(10, 7), new Point(12, 6));
        Rectangle rectangleB_b = new Rectangle(new Point(5, 6), new Point(6, 4));
        Rectangle rectangleB_l = new Rectangle(new Point(3, 11), new Point(5, 10));

        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_t, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_r, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_b, rectangleA));
        assertFalse(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isContainedWithinWithSideOverlap_returnsTrue_whenRectangleBIsContainedWithinRectangleA_AndSideOverlaps() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        Rectangle rectangleB_t = new Rectangle(new Point(6, 12), new Point(8, 11));
        Rectangle rectangleB_r = new Rectangle(new Point(10, 10), new Point(11, 8));
        Rectangle rectangleB_b = new Rectangle(new Point(7, 6), new Point(9, 5));
        Rectangle rectangleB_l = new Rectangle(new Point(4, 9), new Point(5, 7));

        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_t, rectangleA));
        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_r, rectangleA));
        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_b, rectangleA));
        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB_l, rectangleA));
    }

    @Test
    public void test_isContainedWithinWithSideOverlap_returnsTrue_whenRectanglesAreTheSame() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(4, 12), new Point(11, 5));

        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB, rectangleA));
    }

    @Test
    public void test_isContainedWithinWithSideOverlap_returnsTrue_whenRectangleBIsFullyContainedWithinRectangleA() {
        rectangleA = new Rectangle(new Point(4, 12), new Point(11, 5));
        rectangleB = new Rectangle(new Point(6, 9), new Point(9, 8));

        assertTrue(rectangleServiceImpl.isContainedWithinWithSideOverlap(rectangleB, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsSubLine_whenRectangleBIsSubLineAdjacent() {
        rectangleA = new Rectangle(new Point(3, 13), new Point(5, 10));
        Rectangle rectangleB_r = new Rectangle(new Point(5, 12), new Point(7, 11));
        Rectangle rectangleB_l = new Rectangle(new Point(1, 12), new Point(3, 11));
        Rectangle rectangleB_t = new Rectangle(new Point(4, 15), new Point(5, 13));
        Rectangle rectangleB_b = new Rectangle(new Point(3, 10), new Point(4, 8));

        assertEquals(Adjacency.SUB_LINE, rectangleServiceImpl.getAdjacency(rectangleB_r, rectangleA));
        assertEquals(Adjacency.SUB_LINE, rectangleServiceImpl.getAdjacency(rectangleB_l, rectangleA));
        assertEquals(Adjacency.SUB_LINE, rectangleServiceImpl.getAdjacency(rectangleB_t, rectangleA));
        assertEquals(Adjacency.SUB_LINE, rectangleServiceImpl.getAdjacency(rectangleB_b, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsProper_whenRectangleBIsProperAdjacent() {
        rectangleA = new Rectangle(new Point(3, 5), new Point(5, 2));
        Rectangle rectangleB_r = new Rectangle(new Point(5, 5), new Point(6, 2));
        Rectangle rectangleB_l = new Rectangle(new Point(2, 5), new Point(3, 2));
        Rectangle rectangleB_t = new Rectangle(new Point(3, 6), new Point(5, 5));
        Rectangle rectangleB_b = new Rectangle(new Point(3, 2), new Point(5, 1));

        assertEquals(Adjacency.PROPER, rectangleServiceImpl.getAdjacency(rectangleB_r, rectangleA));
        assertEquals(Adjacency.PROPER, rectangleServiceImpl.getAdjacency(rectangleB_l, rectangleA));
        assertEquals(Adjacency.PROPER, rectangleServiceImpl.getAdjacency(rectangleB_t, rectangleA));
        assertEquals(Adjacency.PROPER, rectangleServiceImpl.getAdjacency(rectangleB_b, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsPartial_whenRectangleBIsOnRightAndIsPartialAdjacent() {
        rectangleA = new Rectangle(new Point(10, 5), new Point(12, 2));
        Rectangle rectangleB_rt = new Rectangle(new Point(12, 6), new Point(13, 4)); // right-top
        Rectangle rectangleB_rb = new Rectangle(new Point(12, 3), new Point(13, 1)); // right-bottom

        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_rt, rectangleA));
        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_rb, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsPartial_whenRectangleBIsOnLeftAndIsPartialAdjacent() {
        rectangleA = new Rectangle(new Point(10, 5), new Point(12, 2));
        Rectangle rectangleB_lt = new Rectangle(new Point(9, 6), new Point(10, 4)); // left-top
        Rectangle rectangleB_lb = new Rectangle(new Point(9, 3), new Point(10, 1)); // left-bottom

        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_lt, rectangleA));
        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_lb, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsPartial_whenRectangleBIsOnTopAndIsPartialAdjacent() {
        rectangleA = new Rectangle(new Point(10, 5), new Point(12, 2));
        Rectangle rectangleB_tl = new Rectangle(new Point(9, 6), new Point(11, 5)); // top-left
        Rectangle rectangleB_tr = new Rectangle(new Point(11, 6), new Point(13, 5)); // top-right

        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_tl, rectangleA));
        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_tr, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsPartial_whenRectangleBIsOnBottomAndIsPartialAdjacent() {
        rectangleA = new Rectangle(new Point(10, 5), new Point(12, 2));
        Rectangle rectangleB_bl = new Rectangle(new Point(9, 2), new Point(11, 1)); // bottom-left
        Rectangle rectangleB_br = new Rectangle(new Point(11, 2), new Point(13, 1)); // bottom-right

        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_bl, rectangleA));
        assertEquals(Adjacency.PARTIAL, rectangleServiceImpl.getAdjacency(rectangleB_br, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBIsOnTopAndIsDisjointAndSharesAnAxis() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(12, 10));
        Rectangle rectangleB_tl = new Rectangle(new Point(7, 14), new Point(9, 13)); // top-left
        Rectangle rectangleB_tr = new Rectangle(new Point(13, 14), new Point(15, 13)); // top-right

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_tl, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_tr, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBIsOnRightAndIsDisjointAndSharesAnAxis() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(12, 10));
        Rectangle rectangleB_rt = new Rectangle(new Point(12, 16), new Point(13, 14)); // right-top
        Rectangle rectangleB_rb = new Rectangle(new Point(12, 9), new Point(13, 7)); // right-bottom

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_rt, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_rb, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBIsOnBottomAndIsDisjointAndSharesAnAxis() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(12, 10));
        Rectangle rectangleB_bl = new Rectangle(new Point(7, 10), new Point(9, 9)); // bottom-left
        Rectangle rectangleB_br = new Rectangle(new Point(13, 10), new Point(15, 9)); // bottom-right

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_bl, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_br, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBIsOnLeftAndIsDisjointAndSharesAnAxis() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(12, 10));
        Rectangle rectangleB_lt = new Rectangle(new Point(9, 16), new Point(10, 14)); // left-top
        Rectangle rectangleB_lb = new Rectangle(new Point(9, 9), new Point(10, 7)); // left-bottom

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_lt, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_lb, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBExtendsFromBothSides() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(13, 9)); // 3x3 instead of 2x3
        Rectangle rectangleB_lr = new Rectangle(new Point(9, 12), new Point(14, 11)); // left-right
        Rectangle rectangleB_tp = new Rectangle(new Point(11, 14), new Point(12, 8)); // top-bottom

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_lr, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_tp, rectangleA));
    }

    @Test
    public void test_isAdjacent_returnsNone_whenRectangleBIsDisjoint() {
        rectangleA = new Rectangle(new Point(10, 13), new Point(12, 10));
        Rectangle rectangleB_t = new Rectangle(new Point(10, 15), new Point(12, 14));
        Rectangle rectangleB_r = new Rectangle(new Point(13, 13), new Point(14, 10));
        Rectangle rectangleB_b = new Rectangle(new Point(10, 9), new Point(12, 8));
        Rectangle rectangleB_l = new Rectangle(new Point(8, 13), new Point(9, 10));

        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_t, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_r, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_b, rectangleA));
        assertEquals(Adjacency.NONE, rectangleServiceImpl.getAdjacency(rectangleB_l, rectangleA));
    }

}
