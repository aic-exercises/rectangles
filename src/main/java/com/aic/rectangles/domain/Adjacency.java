package com.aic.rectangles.domain;

public enum Adjacency {
    SUB_LINE, PARTIAL, PROPER, NONE
}
