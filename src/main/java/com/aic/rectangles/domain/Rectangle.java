package com.aic.rectangles.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
@AllArgsConstructor
public class Rectangle {

    private Point topLeft;
    private Point bottomRight;

}
