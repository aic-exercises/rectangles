package com.aic.rectangles.service;

import com.aic.rectangles.domain.Adjacency;
import com.aic.rectangles.domain.Point;
import com.aic.rectangles.domain.Rectangle;
import com.aic.rectangles.dto.RectangleResponseDTO;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class RectangleServiceImpl implements RectangleService {

    @Override
    public RectangleResponseDTO compare(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        return RectangleResponseDTO.builder()
                .intersects(isIntersecting(rectangleB, rectangleA))
                .intersectingPoints(getIntersectingPoints(rectangleB, rectangleA))
                .strictlyContainedWithin(isFullyContainedWithin(rectangleB, rectangleA))
                .containedWithinWithSideOverlap(isContainedWithinWithSideOverlap(rectangleB, rectangleA))
                .adjacency(getAdjacency(rectangleB, rectangleA))
                .build();
    }

    public boolean isIntersecting(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        // one rectangle is above other
        if (rectangleB.getTopLeft().getY() < rectangleA.getBottomRight().getY() || rectangleA.getTopLeft().getY() < rectangleB.getBottomRight().getY()) {
            return false;
        }
        // one rectangle is left of other
        if (rectangleB.getTopLeft().getX() > rectangleA.getBottomRight().getX() || rectangleA.getTopLeft().getX() > rectangleB.getBottomRight().getX()) {
            return false;
        }
        if (isFullyContainedWithin(rectangleB, rectangleA)) {
            return false;
        }
        return true;
    }

    public Set<Point> getIntersectingPoints(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        Set<Point> intersectingRectanglePts = new HashSet<>();

        if (isIntersecting(rectangleB, rectangleA)) {
            Point intPtTopLft = new Point(
                    Math.max(rectangleB.getTopLeft().getX(), rectangleA.getTopLeft().getX()),
                    Math.min(rectangleB.getTopLeft().getY(), rectangleA.getTopLeft().getY())
            );
            Point intPtBotRgt = new Point(
                    Math.min(rectangleB.getBottomRight().getX(), rectangleA.getBottomRight().getX()),
                    Math.max(rectangleB.getBottomRight().getY(), rectangleA.getBottomRight().getY())
            );
            Point intPtBotLft = new Point(
                    Math.max(rectangleB.getTopLeft().getX(), rectangleA.getTopLeft().getX()),
                    Math.max(rectangleB.getBottomRight().getY(), rectangleA.getBottomRight().getY())
            );
            Point intPtTopRgt = new Point(
                    Math.min(rectangleB.getBottomRight().getX(), rectangleA.getBottomRight().getX()),
                    Math.min(rectangleB.getTopLeft().getY(), rectangleA.getTopLeft().getY())
            );

            intersectingRectanglePts.addAll(Arrays.asList(intPtTopLft, intPtBotRgt, intPtBotLft, intPtTopRgt));
            intersectingRectanglePts.removeIf(intPt -> isNotEdgePoint(rectangleB, intPt) || isNotEdgePoint(rectangleA, intPt));
        }
        return intersectingRectanglePts;
    }

    public boolean isFullyContainedWithin(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        return rectangleB.getTopLeft().getY() < rectangleA.getTopLeft().getY() && rectangleB.getTopLeft().getX() > rectangleA.getTopLeft().getX()
                && rectangleB.getBottomRight().getY() > rectangleA.getBottomRight().getY() && rectangleB.getBottomRight().getX() < rectangleA.getBottomRight().getX();
    }

    public boolean isContainedWithinWithSideOverlap(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        return rectangleB.getTopLeft().getY() <= rectangleA.getTopLeft().getY() && rectangleB.getTopLeft().getX() >= rectangleA.getTopLeft().getX()
                && rectangleB.getBottomRight().getY() >= rectangleA.getBottomRight().getY() && rectangleB.getBottomRight().getX() <= rectangleA.getBottomRight().getX();
    }

    public Adjacency getAdjacency(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA) {
        // X_Right, X_Left
        if (rectangleB.getBottomRight().getX() == rectangleA.getTopLeft().getX() || rectangleB.getTopLeft().getX() == rectangleA.getBottomRight().getX()) {
            if (rectangleB.getTopLeft().getY() == rectangleA.getTopLeft().getY() && rectangleB.getBottomRight().getY() == rectangleA.getBottomRight().getY()) {
                return Adjacency.PROPER;
            }
            if (rectangleB.getTopLeft().getY() <= rectangleA.getTopLeft().getY() && rectangleB.getBottomRight().getY() >= rectangleA.getBottomRight().getY()) {
                return Adjacency.SUB_LINE;
            }
            if ((rectangleB.getTopLeft().getY() <= rectangleA.getTopLeft().getY() && rectangleB.getTopLeft().getY() >= rectangleA.getBottomRight().getY())
                    || (rectangleB.getBottomRight().getY() <= rectangleA.getTopLeft().getY() && rectangleB.getBottomRight().getY() >= rectangleA.getBottomRight().getY())) {
                return Adjacency.PARTIAL;
            }
        }
        // Y_Top, Y_Bottom
        if (rectangleB.getBottomRight().getY() == rectangleA.getTopLeft().getY() || rectangleB.getTopLeft().getY() == rectangleA.getBottomRight().getY()) {
            if (rectangleB.getTopLeft().getX() == rectangleA.getTopLeft().getX() && rectangleB.getBottomRight().getX() == rectangleA.getBottomRight().getX()) {
                return Adjacency.PROPER;
            }
            if (rectangleB.getTopLeft().getX() >= rectangleA.getTopLeft().getX() && rectangleB.getBottomRight().getX() <= rectangleA.getBottomRight().getX()) {
                return Adjacency.SUB_LINE;
            }
            if ((rectangleB.getTopLeft().getX() >= rectangleA.getTopLeft().getX() && rectangleB.getTopLeft().getX() <= rectangleA.getBottomRight().getX())
                    || (rectangleB.getBottomRight().getX() >= rectangleA.getTopLeft().getX() && rectangleB.getBottomRight().getX() <= rectangleA.getBottomRight().getX())) {
                return Adjacency.PARTIAL;
            }
        }
        return Adjacency.NONE;
    }

    public boolean isNotEdgePoint(@NonNull Rectangle rectangle, @NonNull Point point) {
        return (rectangle.getTopLeft().getX() != point.getX() && rectangle.getBottomRight().getX() != point.getX()
                && rectangle.getTopLeft().getY() != point.getY() && rectangle.getBottomRight().getY() != point.getY());
    }
}
