package com.aic.rectangles.service;

import com.aic.rectangles.domain.Adjacency;
import com.aic.rectangles.domain.Point;
import com.aic.rectangles.domain.Rectangle;
import com.aic.rectangles.dto.RectangleResponseDTO;
import lombok.NonNull;

import java.util.Set;

public interface RectangleService {

    public RectangleResponseDTO compare(@NonNull Rectangle secondaryRectangle, @NonNull Rectangle primaryRectangle);

    public boolean isIntersecting(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA);

    public Set<Point> getIntersectingPoints(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA);

    public boolean isFullyContainedWithin(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA);

    public boolean isContainedWithinWithSideOverlap(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA);

    public Adjacency getAdjacency(@NonNull Rectangle rectangleB, @NonNull Rectangle rectangleA);

    public boolean isNotEdgePoint(@NonNull Rectangle rectangle, @NonNull Point point);

}
