package com.aic.rectangles.controller;

import com.aic.rectangles.dto.RectangleRequestDTO;
import com.aic.rectangles.dto.RectangleResponseDTO;
import com.aic.rectangles.service.RectangleService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api/v1/rectangle")
public class RectangleController {

    private final RectangleService rectangleService;

    @PostMapping(value = "/compare", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public RectangleResponseDTO compare(@NonNull @RequestBody RectangleRequestDTO requestDTO) {

        return rectangleService.compare(requestDTO.getRectangleB(), requestDTO.getRectangleA());
    }

}
