package com.aic.rectangles.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping({"/"})
public class HomeController {

    @Value("${app.version}")
    private String appVersion;

    @GetMapping(value = "", produces = MediaType.APPLICATION_JSON_VALUE)
    public String home() {
        return "Rectangle Service v" + appVersion.split("-")[0];
    }

}
