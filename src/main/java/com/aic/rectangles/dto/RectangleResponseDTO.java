package com.aic.rectangles.dto;

import com.aic.rectangles.domain.Adjacency;
import com.aic.rectangles.domain.Point;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RectangleResponseDTO {

    private boolean intersects;
    private Set<Point> intersectingPoints;
    private boolean strictlyContainedWithin;
    private boolean containedWithinWithSideOverlap;
    private Adjacency adjacency;

}
