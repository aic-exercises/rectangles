package com.aic.rectangles.dto;

import com.aic.rectangles.domain.Rectangle;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RectangleRequestDTO {

    @NonNull
    private Rectangle rectangleA;
    @NonNull
    private Rectangle rectangleB;

}
